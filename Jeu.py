#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
Résoud Sudoku -Python 3
"""

import os
from data_sudoku import Sudoku
from PyQt5 import QtGui
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import numpy as np
import sys

HERE = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0,HERE)
os.chdir(HERE)

Solve_version = "2"

# ============================================================================
# Classes
# ============================================================================
class Window(QMainWindow): 
    def __init__(self): 
        # Initialisation de la fenêtre
        super().__init__()
        self.widget = QWidget()
        self.setCentralWidget(self.widget)

        #Les labels
        Label = QLabel("Entrez 1 nombre par case.",self)
        Label.setAlignment(Qt.AlignmentFlag.AlignCenter)


# utilisation d'un QGridLayout pour organiser les widgets
        self.grid = QGridLayout()
        
        # création des cases modifiables
        self.cases = []
        for i in range(9):
            ligne_cases = []
            for j in range(9):
                case = QLineEdit()
                case.setMaxLength(1)
                case.setAlignment(Qt.AlignCenter)
                case.setFrame(True)
                case.setStyleSheet("QLineEdit { border: 1px solid black; font-size: 20px; }")
                self.grid.addWidget(case, i, j)
                ligne_cases.append(case)
            self.cases.append(ligne_cases)

#les boutons Résoudre, Nouveau, Quitter
        self.solve_button = QPushButton("Résoudre",self)
        self.solve_button.clicked.connect(self.fResoudre)

        self.new_button = QPushButton("Nouveau",self)
        self.new_button.clicked.connect(self.freset)
        
        self.close_button = QPushButton("Quitter",self)
        self.close_button.clicked.connect(self.fclose)


                #La fenetre et ses details
        self.win_icon = QtGui.QIcon('icon.png')
        self.setWindowIcon(self.win_icon)
        self.setWindowTitle("Mon petit solveur de Sudoku |  Version: "+ Solve_version)
        self.resize(700, 500)

        self.vbox = QVBoxLayout()
        self.vbox.setContentsMargins(25, 15, 25, 15)
        self.vbox.addWidget(Label)
        self.vbox.addLayout(self.grid)
        self.vbox.addWidget(self.solve_button)
        self.vbox.addWidget(self.new_button)
        self.vbox.addWidget(self.close_button)
        self.widget.setLayout(self.vbox)

    def fResoudre(self):
        tableau = np.zeros((9, 9))
        for i in range(9):
            for j in range(9):
                val = self.cases[i][j].text()
                if val.isdigit() and int(val) in range(1,10):
                    val = int(val)
                else:
                    val = 0
                tableau[i,j] = val
         # on resoud le sudoku
        mSudok = Sudoku(tableau)
        mSudok.solve()
        for i in range(9):
            for j in range(9):
                self.cases[i][j].setText(str(int(mSudok.solution[i, j])))
                self.cases[i][j].setStyleSheet("color: blue;")

    def freset(self):
        """
        on reinitialise le tableau d'entrees (Entry)
        """
        for i in range(9):
            for j in range(9):
                self.cases[i][j].setText("")
        return

    def fclose(self):
        winw = Window()
        winw.close()
        app.exit()

# ============================================================================
# Fonctions
# ============================================================================
def isValid(val):

    try:
        val=int(val)
        if val>0 and val<10:
            return True
        else:
            return False
    except:
        return False

# Création de l'application
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Window()
    window.show()
    
    sys.exit(app.exec_())
