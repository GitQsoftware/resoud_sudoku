# Résolveur de Sudoku

* **Website:** http://linuxpi.rf.gd
* **Email:** qsoftware at proton.me
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2021-2023

----

#### REQUIREMENTS
* [Python3](https://www.python.org): programming language
* [Qt](https://www.qt.io): "toolkit" comprehensive (graphical user interface and a lot of things)
* [PyQt](https://riverbankcomputing.com): link between Python and Qt

#### Other libraries used and other stuff
* [Numpy](https://numpy.org/)
* [Flaticon](https://flaticon.com): for the logo
